import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.Random;

public class Main {
    WebDriver driver = new FirefoxDriver();

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\UFED-PC\\Desktop\\ItHillel\\ithilleluserdata\\drivers\\geckodriver.exe");
    }

    @Test
    public void openUserData() throws InterruptedException {
        driver.get("https://user-data.hillel.it/html/registration.html");
        int number = new Random().nextInt(100);
        String first_name = "Ivan" + number;
        String last_name = "Ivanov" + number;
        String field_work_phone = "048777" + 0001 + number;
        String field_phone = "38096" + (1234567 + number);
        String field_email = "ivan" + number + "@mail.ru";
        String field_password = "Password" + number;
        String[] gender = {"male", "female"};
        String[] position = {"manager", "qa", "developer"};

        driver.findElement(By.cssSelector(".registration")).click();
        driver.findElement(By.cssSelector("#first_name")).sendKeys(first_name);
        driver.findElement(By.cssSelector("#last_name")).sendKeys(last_name);
        driver.findElement(By.cssSelector("#field_work_phone")).sendKeys(field_work_phone);
        driver.findElement(By.cssSelector("#field_phone")).sendKeys(field_phone);
        driver.findElement(By.cssSelector("#field_email")).sendKeys(field_email);
        driver.findElement(By.cssSelector("#field_password")).sendKeys(field_password);
        driver.findElement(By.cssSelector("#" + gender[0])).click();
        WebElement selectItem = driver.findElement(By.cssSelector("#position"));
        Select select = new Select(selectItem);
        select.selectByValue(position[1]);
        driver.findElement(By.cssSelector("#button_account")).click();
    }

    @After
    public void closeUserData(){
        driver.quit();
    }
}